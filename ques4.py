def factorial(n):
    """Calculates n!
    """
    result = 1
    for i in range(1, n+1):
        result *= i
    return result

def permutation(n, k):
    """Calculates n k permutation by using its factorial definition
    """
    #n! / (n-k)! k!
    return factorial(n) / factorial(n-k)

result = permutation(6, 4)
print(result)
